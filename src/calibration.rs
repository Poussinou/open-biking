use crate::timer::TimeMs;
use crate::utils::{Cadence, Resistance, MAX_RES, MIN_RES};

struct Coefficient {
    square: f32,
    linear: f32,
}

impl Coefficient {
    pub const fn new(square: f32, linear: f32) -> Self {
        Coefficient { square, linear }
    }

    pub fn interpolate(&self, cad: Cadence) -> f32 {
        let cad: f32 = cad.into();
        (self.square * cad + self.linear) * cad
    }
}

const CALIBRATION_SIZE: Resistance = MAX_RES - MIN_RES + 1;
const CALIBRATION: [Coefficient; CALIBRATION_SIZE as usize] = [
    // Res 1
    Coefficient::new(-0.00024315810195804774, 0.13273282142051962),
    // res 2
    Coefficient::new(-6.838044877888194e-06, 0.14662733826099317),
    // res 3
    Coefficient::new(0.00032451727600534523, 0.136329709890235),
    // res 4
    Coefficient::new(0.0005159824625714685, 0.1307642392210212),
    // res 5
    Coefficient::new(0.0005570107314471598, 0.15100020867926364),
    // res 6
    Coefficient::new(0.0007248325237243503, 0.15750214413013863),
    // res 7
    Coefficient::new(0.0008600867022895593, 0.17082241881981272),
    // res 8
    Coefficient::new(0.0008442085233536512, 0.19265200157440487),
    // res 9
    Coefficient::new(0.0007963422244728902, 0.21904336941483316),
    // res 10
    Coefficient::new(0.001274078018440652, 0.19874017793284957),
    // res 11
    Coefficient::new(0.0013648269821989881, 0.20536380558808295),
    // res 12
    Coefficient::new(0.001628371626194114, 0.20347152846979943),
    // res 13
    Coefficient::new(0.0018221621803410947, 0.20728026506276173),
    // res 14
    Coefficient::new(0.0016458614725067822, 0.22471237598230354),
    // res 15
    Coefficient::new(0.0023006351464284736, 0.19163020456244473),
    // res 16
    Coefficient::new(0.002971065629441605, 0.1590684544794525),
];

/// Use a f32 in return in order to avoid rounding issues when summing lot of elements
pub fn get_kcalories(res: Resistance, cad: Cadence, delta_time_ms: TimeMs) -> f32 {
    if res > MAX_RES || res < MIN_RES {
        panic!("Should not happen {}", res);
    }
    let res: usize = res.into();
    let delta_time_ms: f32 = delta_time_ms as f32;
    CALIBRATION[res - 1].interpolate(cad) * delta_time_ms / (1000. * 60.)
}
