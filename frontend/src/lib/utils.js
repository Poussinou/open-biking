import { writable, get } from 'svelte/store';
import { toast } from '@zerodevx/svelte-toast';
import { fetch as tauri_fetch } from '@tauri-apps/plugin-http';
import { getTauriVersion } from '@tauri-apps/api/app';

export const current_user = writable('');
export const backend = writable(null);
export const tauri = writable(null);

export async function call_backend(...args) {
	while (get(tauri) == null) {
		await new Promise((r) => setTimeout(r, 100));
	}
	args[0] = get_server() + args[0];
	if (get(tauri)) {
		return tauri_fetch(...args);
	} else {
		return fetch(...args);
	}
}

export async function reset_backend() {
	backend.set(null);
	localStorage.removeItem('backend_address');
	await call_backend('/backend', {
		method: 'DELETE'
	});
	init_backend();
}

export async function set_running_with_tauri() {
	try {
		const version = await getTauriVersion();
		console.log('With tauri', version);
		tauri.set(true);
	} catch {
		console.log('Without tauri');
		tauri.set(false);
	}
}

export async function init_backend() {
	// Check if running with tauri
	await set_running_with_tauri();

	// Already init
	let local_host = window.location.host.includes('localhost');
	if (get(backend) !== null || !local_host) {
		return;
	}

	// Check if already set
	const response_get = await call_backend('/backend');
	if (response_get.ok) {
		backend.set(response_get.text());
		return;
	}

	// Ask for the backend
	let backend_address = localStorage.getItem('backend_address');
	if (backend_address === null) {
		backend_address = prompt('Please enter the backend server address');
		if (backend_address === null) {
			return;
		}
	}

	let encoded = encodeURIComponent(backend_address);
	const response = await call_backend(`/backend/${encoded}`, {
		method: 'POST'
	});
	if (!response.ok) {
		toast.push('Failed to set the backend');
		return;
	}

	localStorage.setItem('backend_address', backend_address);
	backend.set(backend_address);
}

export function get_time_from_ms(value) {
	let sec = Math.trunc(value / 1000);
	let min = Math.trunc(sec / 60);
	sec -= 60 * min;

	// Deal with negative numbers
	let sign = '';
	if (sec < 0) {
		sec = -sec;
		sign = '-';
	}

	// Add 0
	sec = String(sec).padStart(2, '0');
	min = String(min).padStart(2, '0');
	return `${sign}${min}:${sec}`;
}

export function get_server() {
	let current = window.location.origin;
	if (current.includes('localhost')) return 'http://localhost:8000';
	else return current;
}
